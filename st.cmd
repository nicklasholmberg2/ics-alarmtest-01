#!/usr/bin/env iocsh.bash
#epicsEnvSet("SYS", "Instr1")
#epicsEnvSet("EPICS_DB_INCLUDE_PATH", "/home/nicklasholmberg2/git/nss-alarm-configs-test/")

#dbLoadRecords("building.db")
#dbLoadRecrods("tech_group.db")
#dbLoadRecords("/home/nicklasholmberg2/git/nss-alarm-configs-test/alarm_test.db")
require(cntpstats)
epicsEnvSet("SYS", "alarm")
epicsEnvSet("DEV", "test")
iocshLoad("$(cntpstats_DIR)/cntpstats.iocsh","SYS=$(SYS), DEV=$(DEV)")

dbLoadRecords("./alarm_test.db")

iocInit()

dbpf Instr1:BM1:temp 0
dbpf Instr1:BM2:temp 0
dbpf Instr1:BM3:temp 0
dbpf Instr1:Chopper2:speed 0
dbpf Instr1:Chopper3:speed 0
dbpf Instr1:Chopper1:speed
dbpf Instr1:Chopper1:speed 0
dbpf PnP:Device01:temp 0

#EOF
